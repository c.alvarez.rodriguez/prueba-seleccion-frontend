# TACTECH FRONTEND TEST

To execute this project you need follow next steps:

## ¿How to run it?

1. Clone the repo: git clone https://gitlab.com/c.alvarez.rodriguez/prueba-seleccion-frontend.git
2. Enter the directory: cd prueba-seleccion-frontend
3. Init .env: cp .env.example .env.
4. Install the project: npm install
5. Run the project: npm run start

### Other way to run it...

1. Clone the repo: git clone https://gitlab.com/c.alvarez.rodriguez/prueba-seleccion-frontend.git
2. Enter the directory: cd prueba-seleccion-frontend
3. Init .env: cp .env.example .env.
4. Run the project: docker-compose up

## For test it...

### Login

#### Success

user: eve.holt@reqres.in<br/>
password: cityslicka

#### Unsuccessfull

user: any valid email<br/>
password: any password (min 3 characters)

### Register

#### Success

user: eve.holt@reqres.in<br/>
password: pistol

#### Unsuccessfull

user: any valid email<br/>
password: any password (min 3 characters)

## CI/CD Netlify

https://tactech-cristian-alvarez.netlify.app/login

## Port

Client: http://localhost:3000
