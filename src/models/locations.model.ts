export interface IInfo {
  count: number;
  pages: number;
  next: string;
  prev?: any;
}

export interface IResultLocation {
  id: number;
  name: string;
  type: string;
  dimension: string;
  residents: string[];
  url: string;
  created: Date;
}

export interface ILocationsResponse {
  info: IInfo;
  results: IResultLocation[];
}