export interface IUser {
  email: String
  password: String
}

export interface ILoginResponse {
  token: string
}
