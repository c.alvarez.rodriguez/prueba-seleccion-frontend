    export interface IInfo {
        count: number;
        pages: number;
        next: string;
        prev?: any;
    }

    export interface IResultEpisode {
        id: number;
        name: string;
        air_date: string;
        episode: string;
        characters: string[];
        url: string;
        created: Date;
    }

    export interface IEpisodesResponse {
        info: IInfo;
        results: IResultEpisode[];
    }