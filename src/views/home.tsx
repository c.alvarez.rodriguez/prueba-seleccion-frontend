import React, { useState } from 'react'
import { Tabs, Tab, AppBar } from '@material-ui/core'
import styled from 'styled-components'
import CharactersTab from '../components/tabs/charactersTab'
import LocationTab from 'src/components/tabs/locationsTab'
import EpisodesTab from 'src/components/tabs/episodesTab'

const HomeStyled = styled.div`
  .appbar {
    background: black;
    max-width: 1312px;
    margin: auto;
  }
  .backgrund-tab {
    background: black;
  }
`

interface IProps {
  children: JSX.Element | JSX.Element[] | string | string[]
  value: number
  index: number
}

const TabPanel = (props: IProps) => {
  const { children, value, index } = props
  return <div>{value === index && <h1>{children}</h1>}</div>
}

const Home = () => {
  const [value, setValue] = useState(0)

  const handleTabs = (val: number) => {
    setValue(val)
  }

  return (
    <HomeStyled>
      <div className="backgrund-tab">
        <AppBar className="appbar" position="static">
          <Tabs value={value} onChange={(event, val) => handleTabs(val)}>
            <Tab label="Characters" />
            <Tab label="Episodes" />
            <Tab label="Locations" />
          </Tabs>
        </AppBar>
      </div>
      <TabPanel value={value} index={0}>
        <CharactersTab />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <EpisodesTab />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <LocationTab />
      </TabPanel>
    </HomeStyled>
  )
}

export default Home
