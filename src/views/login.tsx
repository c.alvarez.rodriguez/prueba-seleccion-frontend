import React, { useState } from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import RYMLogo from '../assets/Rick_and_Morty_logo.png'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import { Button, Grid, Link as LinkMaterial, Snackbar, TextField } from '@material-ui/core'
import { Formik, Field, ErrorMessage, Form } from 'formik'
import styled from 'styled-components'
import { LoginSchema } from 'src/utils/validations'
import { Link, Redirect } from 'react-router-dom'
import { postLogin } from 'src/services/public.service'
import { IUser } from 'src/models/users.mode'
import Alert from '@material-ui/lab/Alert/Alert'

const LoginStyle = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  text-align: center;
  margin-top: 100px;
  img {
    width: 80%;
    margin-left: auto;
    margin-right: auto;
  }
  .error {
    color: red;
    position: absolute;
  }
  button {
    background-color: #30b0c2;
    margin: 20px 0;
    font-weight: bold;
    color: white;
    transition: 0.3s;
    :hover {
      color: black;
      background-color: #c4ec57;
    }
  }
  .input {
    margin: 15px 0;
  }
`

const Copyright = () => {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <LinkMaterial color="inherit" href="https://www.linkedin.com/in/cristian-%C3%A1lvarez-rodr%C3%ADguez-7bb3311b2/">
        Cristian Alvarez
      </LinkMaterial>
      {' 2020.'}
    </Typography>
  )
}

const initialValues = {
  email: '',
  password: '',
}

const Login = () => {
  const [logStatus, setLogStatus] = useState(false)
  const [open, setOpen] = useState(false)

  const handleSubmit = (values: IUser) => {
    postLogin(values)
      .then((response) => {
        localStorage.setItem('token', response)
        setLogStatus(true)
      })
      .catch((error) => setOpen(true))
  }

  return (
    <>
      <Container component="main" maxWidth="xs">
        <LoginStyle>
          <CssBaseline />
          <img alt="RICK AND MORTY" src={RYMLogo} />
          <Typography component="h1" variant="h5">
            Login
          </Typography>
          <Formik initialValues={initialValues} validationSchema={LoginSchema} onSubmit={handleSubmit}>
            <Form>
              <Field
                as={TextField}
                variant="outlined"
                margin="normal"
                fullWidth
                className="input"
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                helperText={<ErrorMessage name="email">{(msg) => <div className="error">{msg}</div>}</ErrorMessage>}
              />
              <Field
                as={TextField}
                variant="outlined"
                className="input"
                margin="normal"
                fullWidth
                id="password"
                label="Password"
                name="password"
                type="password"
                autoComplete="password"
                helperText={<ErrorMessage name="password">{(msg) => <div className="error">{msg}</div>}</ErrorMessage>}
              />
              <Button type="submit" fullWidth variant="contained">
                Login
              </Button>
              {logStatus ? <Redirect to="/home" /> : null}
              <Link to="/register">{"Don't have an account? Register"}</Link>
            </Form>
          </Formik>
          <Box mt={8}>
            <Copyright />
          </Box>
        </LoginStyle>
      </Container>
      <Grid item xs={12} md={3}>
        <Snackbar
          anchorOrigin={{ horizontal: 'center', vertical: 'bottom' }}
          open={open}
          onClose={() => setOpen(false)}
          autoHideDuration={4000}
        >
          <Alert severity="error">{'The name or password is incorrect. Please, try again.'}</Alert>
        </Snackbar>
      </Grid>
    </>
  )
}

export default Login
