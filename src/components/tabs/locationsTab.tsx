import { CircularProgress } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { ILocationsResponse, IResultLocation, IInfo } from 'src/models/locations.model'
import { getLocations } from 'src/services/private.service'
import styled from 'styled-components'
import LocationCard from '../cards/locationCard'
import CustomPagination from '../layouts/pagination'
import Wrapper from '../wrappers/wrapper'

const LocationTabStyle = styled.div`
  .list {
    display: grid;
    grid-auto-flow: columns;
    grid-column-gap: 40px;
    grid-row-gap: 2.3em;
    grid-template-columns: repeat(auto-fill, minMax(0, 270px));
    justify-content: center;
    justify-items: space-between;
    padding: 3em 0;
  }
  .center {
    display: flex;
    justify-content: center;
    padding-top: 30px;
  }
  .pagination {
    display: flex;
    justify-content: center;
    padding-bottom: 50px;
  }
  .loader {
    display: flex;
    justify-content: center;
    width: 100vw;
    height: calc(100vh - 80px);
    align-items: center;
  }
`

const LocationTab = () => {
  const [pageInfo, setPageInfo] = useState<IInfo>()
  const [locations, setLocations] = useState<IResultLocation[]>([])
  const [loader, setLoader] = useState(false)
  const [currentPage, setCurrentPage] = useState(1)

  useEffect(() => {
    const getLocationsList = async () => {
      const response: ILocationsResponse = await getLocations(currentPage)
      setPageInfo(response.info)
      setLocations(response.results)
      setLoader(true)
    }
    getLocationsList()
  }, [currentPage])

  const handlePagination = (pageNumber: number) => {
    setCurrentPage(pageNumber)
  }

  return (
    <LocationTabStyle>
      <Wrapper>
        {!loader ? (
          <div className="loader">
            <CircularProgress color="inherit" size={100} />
          </div>
        ) : (
          <>
            {locations.length > 0 ? (
              <>
                <div className="list">
                  {locations.map((location: IResultLocation, index: number) => (
                    <LocationCard key={index} location={location} />
                  ))}
                </div>
                <div className="pagination">
                  {pageInfo?.pages && <CustomPagination pages={pageInfo.pages} handlePagination={handlePagination} />}
                </div>
              </>
            ) : (
              <h1>Location was not found.</h1>
            )}
          </>
        )}
      </Wrapper>
    </LocationTabStyle>
  )
}

export default LocationTab
