import { CircularProgress } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { ICharactersResponse, IInfo, IResultCharacter } from 'src/models/characters.model'
import { getCharacters } from 'src/services/private.service'
import styled from 'styled-components'
import CharacterCard from '../cards/characterCard'
import CustomPagination from '../layouts/pagination'
import Wrapper from '../wrappers/wrapper'

const CharactersTabStyle = styled.div`
  .list {
    display: grid;
    grid-auto-flow: columns;
    grid-column-gap: 40px;
    grid-row-gap: 2.3em;
    grid-template-columns: repeat(auto-fill, minMax(0, 270px));
    justify-content: center;
    justify-items: space-between;
    padding: 3em 0;
  }
  .center {
    display: flex;
    justify-content: center;
    padding-top: 30px;
  }
  .pagination {
    display: flex;
    justify-content: center;
    padding-bottom: 50px;
  }
  .loader {
    display: flex;
    justify-content: center;
    width: 100vw;
    height: calc(100vh - 80px);
    align-items: center;
  }
`

const CharactersTab = () => {
  const [pageInfo, setPageInfo] = useState<IInfo>()
  const [characters, setCharacters] = useState<IResultCharacter[]>([])
  const [loader, setLoader] = useState(false)
  const [currentPage, setCurrentPage] = useState(1)

  useEffect(() => {
    const getCharactersList = async () => {
      const response: ICharactersResponse = await getCharacters(currentPage)
      setPageInfo(response.info)
      setCharacters(response.results)
      setLoader(true)
    }
    getCharactersList()
  }, [currentPage])

  const handlePagination = (pageNumber: number) => {
    setCurrentPage(pageNumber)
  }

  return (
    <CharactersTabStyle>
      <Wrapper>
        {!loader ? (
          <div className="loader">
            <CircularProgress color="inherit" size={100} />
          </div>
        ) : (
          <>
            {characters.length > 0 ? (
              <>
                <div className="list">
                  {characters.map((character: IResultCharacter, index: number) => (
                    <CharacterCard key={index} character={character} />
                  ))}
                </div>
                <div className="pagination">
                  {pageInfo?.pages && <CustomPagination pages={pageInfo.pages} handlePagination={handlePagination} />}
                </div>
              </>
            ) : (
              <h1>Character was not found.</h1>
            )}
          </>
        )}
      </Wrapper>
    </CharactersTabStyle>
  )
}

export default CharactersTab
