import { CircularProgress } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { IEpisodesResponse, IResultEpisode, IInfo } from 'src/models/episodes.model'
import { getEpisodes } from 'src/services/private.service'
import styled from 'styled-components'
import EpisodeCard from '../cards/episodeCard'
import CustomPagination from '../layouts/pagination'
import Wrapper from '../wrappers/wrapper'

const EpisodesTabStyle = styled.div`
  .list {
    display: grid;
    grid-auto-flow: columns;
    grid-column-gap: 40px;
    grid-row-gap: 2.3em;
    grid-template-columns: repeat(auto-fill, minMax(0, 270px));
    justify-content: center;
    justify-items: space-between;
    padding: 3em 0;
  }
  .center {
    display: flex;
    justify-content: center;
    padding-top: 30px;
  }
  .pagination {
    display: flex;
    justify-content: center;
    padding-bottom: 50px;
  }
  .loader {
    display: flex;
    justify-content: center;
    width: 100vw;
    height: calc(100vh - 80px);
    align-items: center;
  }
`

const EpisodesTab = () => {
  const [pageInfo, setPageInfo] = useState<IInfo>()
  const [episodes, setEpisodes] = useState<IResultEpisode[]>([])
  const [loader, setLoader] = useState(false)
  const [currentPage, setCurrentPage] = useState(1)

  useEffect(() => {
    const getEpisodesList = async () => {
      const response: IEpisodesResponse = await getEpisodes(currentPage)
      setPageInfo(response.info)
      setEpisodes(response.results)
      setLoader(true)
    }
    getEpisodesList()
  }, [currentPage])

  const handlePagination = (pageNumber: number) => {
    setCurrentPage(pageNumber)
  }

  return (
    <EpisodesTabStyle>
      <Wrapper>
        {!loader ? (
          <div className="loader">
            <CircularProgress color="inherit" size={100} />
          </div>
        ) : (
          <>
            {episodes.length > 0 ? (
              <>
                <div className="list">
                  {episodes.map((episode: IResultEpisode, index: number) => (
                    <EpisodeCard key={index} episode={episode} />
                  ))}
                </div>
                <div className="pagination">
                  {pageInfo?.pages && <CustomPagination pages={pageInfo.pages} handlePagination={handlePagination} />}
                </div>
              </>
            ) : (
              <h1>Episode was not found.</h1>
            )}
          </>
        )}
      </Wrapper>
    </EpisodesTabStyle>
  )
}

export default EpisodesTab
