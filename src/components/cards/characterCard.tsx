import React from 'react'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'
import DefaultCharacterImage from '../../assets/uknown.jpg'
import { IResultCharacter } from '../../models/characters.model'
import { Card, CardActionArea, CardContent, CardMedia, Typography } from '@material-ui/core'
import Moment from 'react-moment'
import { calendarStrings } from 'src/utils/formats'

const CharacterCardStyle = styled.div`
  border-radius: 10px;
  box-shadow: 0 0 7px 2px rgb(0, 0, 0, 0.2);
  cursor: pointer;
  margin-bottom: 1em;
  text-align: left;
  background: white;
  &:hover .details {
    border-radius: 0 0 5px 5px;
    border: 1px solid black;
    border-top: none;
  }
  .root {
    max-width: 345;
  }
  .created {
    padding-top: 15px;
  }
  .details {
    height: 130px;
    border: 1px solid transparent;
    border-top: none;
    padding: 1.5em;
    transition: 0.3s border;
  }
`
interface IProps {
  character: IResultCharacter
}

const CharacterCard = (props: IProps) => {
  const { id, name, image, gender, created } = props.character

  const history = useHistory()

  const handleClick = () => {
    history.push(`/character/${id}`)
  }

  return (
    <CharacterCardStyle onClick={() => handleClick()}>
      <Card className="root">
        <CardActionArea>
          <CardMedia component="img" alt="Character" height="350" image={image ? image : DefaultCharacterImage} title="Character" />
          <CardContent className="details">
            <Typography gutterBottom variant="h5" component="h2">
              {name}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              <strong>Gender: </strong>
              {gender ? gender : ' No specified'}
            </Typography>
            <Typography className="created" variant="body2" color="textSecondary" component="p">
              {created ? <Moment calendar={calendarStrings}>{created}</Moment> : ' No specified'}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </CharacterCardStyle>
  )
}

export default CharacterCard
