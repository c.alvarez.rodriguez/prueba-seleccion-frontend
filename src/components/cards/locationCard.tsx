import { Card, CardActionArea, CardContent, Typography } from '@material-ui/core'
import React from 'react'
import Moment from 'react-moment'
import { useHistory } from 'react-router-dom'
import { calendarStrings } from 'src/utils/formats'
import styled from 'styled-components'
import { IResultLocation } from '../../models/locations.model'

const LocationCardStyle = styled.div`
  border-radius: 10px;
  box-shadow: 0 0 7px 2px rgb(0, 0, 0, 0.2);
  cursor: pointer;
  margin-bottom: 1em;
  text-align: left;
  background: white;
  &:hover .details {
    border-radius: 5px;
    border: 1px solid black;
  }
  .root {
    max-width: 345;
  }
  .created {
    padding-top: 10px;
  }
  .details {
    height: 100px;
    border: 1px solid transparent;
    padding: 1.5em 0.5em;
    transition: 0.3s border;
  }
`
interface IProps {
  location: IResultLocation
}

const LocationCard = (props: IProps) => {
  const { id, name, type, created } = props.location

  const history = useHistory()

  const handleClick = () => {
    history.push(`/location/${id}`)
  }

  return (
    <LocationCardStyle onClick={() => handleClick()}>
      <Card className="root">
        <CardActionArea>
          <CardContent className="details">
            <Typography gutterBottom variant="h5" component="h2">
              {name}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              <strong>Type: </strong>
              {type ? type : ' No specified'}
            </Typography>
            <Typography className="created" variant="body2" color="textSecondary" component="p">
              {created ? <Moment calendar={calendarStrings}>{created}</Moment> : ' No specified'}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </LocationCardStyle>
  )
}

export default LocationCard
