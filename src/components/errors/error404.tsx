import { Button } from '@material-ui/core'
import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const Error404Style = styled.div`
  max-width: 700px;
  padding: 50px;
  display: flex;
  flex-direction: column;
  margin-left: auto;
  margin-right: auto;
  .link {
    text-decoration: none;
  }
  h1 {
    font-size: 80px;
    text-align: center;
    margin: 20px 0;
  }
  h2 {
    font-size: 50px;
    text-align: center;
  }
  p {
    font-size: 20px;
  }
  button {
    background-color: #30b0c2;
    margin: 20px 0;
    font-weight: bold;
    color: white;
    transition: 0.3s;
    :hover {
      color: black;
      background-color: #c4ec57;
    }
  }
`

const Error404 = () => {
  return (
    <Error404Style>
      <h1>404</h1>
      <h2>Página not found</h2>
      <p>Sorry, the page you are trying to ingest was not found, it might have been deleted or its name changed.</p>
      <p>Please return to home page.</p>
      <Link className="link" to="/">
        <Button type="submit" fullWidth variant="contained">
          Return to Home
        </Button>
      </Link>
    </Error404Style>
  )
}

export default Error404
