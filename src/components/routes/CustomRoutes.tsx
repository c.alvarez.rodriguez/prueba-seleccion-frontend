import React from 'react'
import { Redirect, Route } from 'react-router-dom'

export const PrivateRoute = (props: any) => {
  return <>{localStorage.getItem('token') ? <Route {...props} /> : <Redirect to="/login" />}</>
}

export const PublicRoute = (props: any) => {
  return <>{localStorage.getItem('token') ? <Redirect to="/home" /> : <Route {...props} />}</>
}
