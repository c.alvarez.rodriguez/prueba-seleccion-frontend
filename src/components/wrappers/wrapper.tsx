import React from 'react'
import styled from 'styled-components'

const WrapperStyled = styled.div`
  margin: auto;
  max-width: 1312px;
  padding: 0 1rem;
`

interface IProps {
  children: JSX.Element | JSX.Element[] | string | string[]
}

const Wrapper = ({ children }: IProps) => {
  return <WrapperStyled>{children}</WrapperStyled>
}

export default Wrapper
