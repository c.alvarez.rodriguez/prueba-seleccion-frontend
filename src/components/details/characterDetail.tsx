import { Card, CircularProgress, Grid, List, ListItem } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { IResultCharacter } from 'src/models/characters.model'
import DefaultCharacterImage from '../../assets/uknown.jpg'
import { getCharacterById } from 'src/services/private.service'
import styled from 'styled-components'
import Error404 from '../errors/error404'
import Wrapper from '../wrappers/wrapper'
import { Link } from 'react-router-dom'

const CharacterDetailStyle = styled.div`
  padding-bottom: 3em;
  .title {
    padding-left: 10px;
  }
  .back {
    background: #30b0c2;
    padding: 0.7em 2.2em;
    border-radius: 5px;
    border: none;
    margin: 3em 0;
    color: white;
    transition: 0.3s;
    :hover {
      cursor: pointer;
      color: black;
      background-color: #c4ec57;
    }
  }
  .MuiCard-root {
    margin: auto;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
    border-radius: 10px;
  }
  .MuiGrid-container {
    overflow: hidden;
  }
  img {
    width: 100%;
    margin-bottom: 2em;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
  }
  .first {
    background-color: #dadada;
  }
  .second {
    background-color: white;
  }
  .episodes {
    overflow-x: hidden;
    overflow-y: scroll;
    height: 700px;
  }
  .loader {
    display: flex;
    justify-content: center;
    height: calc(100vh - 150px);
    align-items: center;
  }
  @media screen and (min-width: 1024px) {
    .MuiCard-root {
      width: 750px;
      height: 700px;
    }
    .MuiGrid-container {
      width: 750px;
      height: 700px;
    }
    .limit {
      border-left: 1px solid black;
    }
  }
`

const CharacterDetail = ({ match, history }: any) => {
  const [loader, setLoader] = useState(false)
  const [character, setCharacter] = useState<IResultCharacter | null>(null)

  useEffect(() => {
    const getCharacter = async () => {
      const response: IResultCharacter = await getCharacterById(match.params.id)
      setCharacter(response)
      setLoader(true)
    }
    getCharacter()
  }, [match.params.id])

  const handleClick = () => {
    history.goBack()
  }

  return (
    <CharacterDetailStyle>
      <Wrapper>
        {!loader ? (
          <div className="loader">
            <CircularProgress color="inherit" size={100} />
          </div>
        ) : (
          <>
            <button className="back" onClick={() => handleClick()}>
              Back
            </button>
            {!character ? (
              <Error404 />
            ) : (
              <Card>
                <Grid container>
                  <Grid xs={12} md={6}>
                    <Grid>
                      <img src={character.image ? character.image : DefaultCharacterImage} alt="" />
                    </Grid>
                    <Grid style={{ margin: 20 }}>
                      <h2>{character.name}</h2>
                      <div className="grid">
                        <div>
                          <p>
                            <strong>Status: </strong>
                            {character.status ? character.status : 'Not specified'}
                          </p>
                          <p>
                            <strong>Species: </strong>
                            {character.species ? character.species : 'Not specified'}
                          </p>
                          <p>
                            <strong>Type: </strong>
                            {character.type ? character.type : 'Not specified'}
                          </p>
                          <p>
                            <strong>Gender: </strong>
                            {character.gender ? character.gender : 'Not specified'}
                          </p>
                          <p>
                            <strong>Origin: </strong>
                            {character.origin.name ? (
                              <Link to={`/location/${character.origin.url.match(/[0-9]+/)}`}>{character.origin.name}</Link>
                            ) : (
                              'Not specified'
                            )}
                          </p>
                          <p>
                            <strong>Location: </strong>
                            {character.location.name ? (
                              <Link to={`/location/${character.location.url.match(/[0-9]+/)}`}>{character.location.name}</Link>
                            ) : (
                              'Not specified'
                            )}
                          </p>
                        </div>
                      </div>
                    </Grid>
                  </Grid>
                  <Grid xs={12} md={6}>
                    <div className="limit">
                      {character.episode.length > 0 ? (
                        <p>
                          <div className="title">
                            <strong>Episodes: </strong>
                          </div>
                          <List className="episodes">
                            {character.episode.map((element, index) => (
                              <Link to={`/episode/${element.match(/[0-9]+/)}`}>
                                <ListItem className={index % 2 ? 'second' : 'first'} button>{`Episode ${element.match(
                                  /[0-9]+/,
                                )}`}</ListItem>
                              </Link>
                            ))}
                          </List>
                        </p>
                      ) : null}
                    </div>
                  </Grid>
                </Grid>
              </Card>
            )}
          </>
        )}
      </Wrapper>
    </CharacterDetailStyle>
  )
}

export default CharacterDetail
