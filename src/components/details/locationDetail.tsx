import { Card, CircularProgress, Grid, List, ListItem } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { getLocationById } from 'src/services/private.service'
import styled from 'styled-components'
import Error404 from '../errors/error404'
import Wrapper from '../wrappers/wrapper'
import { IResultLocation } from 'src/models/locations.model'
import { Link } from 'react-router-dom'

const LocationDetailStyle = styled.div`
  padding-bottom: 3em;
  .title {
    padding-left: 10px;
  }
  .back {
    background: #30b0c2;
    padding: 0.7em 2.2em;
    border-radius: 5px;
    border: none;
    margin: 3em 0;
    color: white;
    transition: 0.3s;
    :hover {
      cursor: pointer;
      color: black;
      background-color: #c4ec57;
    }
  }
  .MuiCard-root {
    margin: auto;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
    border-radius: 10px;
  }
  .MuiGrid-container {
    overflow: hidden;
  }
  img {
    width: 100%;
    margin-bottom: 2em;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
  }
  .first {
    background-color: #dadada;
  }
  .second {
    background-color: white;
  }
  .characters {
    overflow-x: hidden;
    overflow-y: scroll;
    height: 400px;
  }
  .loader {
    display: flex;
    justify-content: center;
    height: calc(100vh - 150px);
    align-items: center;
  }
  @media screen and (min-width: 1024px) {
    .MuiCard-root {
      width: 750px;
      height: 400px;
    }
    .MuiGrid-container {
      width: 750px;
      height: 400px;
    }
    .limit {
      border-left: 1px solid black;
    }
  }
`

const LocationDetail = ({ match, history }: any) => {
  const [loader, setLoader] = useState(false)
  const [location, setLocation] = useState<IResultLocation | null>(null)

  useEffect(() => {
    const getLocation = async () => {
      const response: IResultLocation = await getLocationById(match.params.id)
      setLocation(response)
      setLoader(true)
    }
    getLocation()
  }, [match.params.id])

  const handleClick = () => {
    history.goBack()
  }

  return (
    <LocationDetailStyle>
      <Wrapper>
        {!loader ? (
          <div className="loader">
            <CircularProgress color="inherit" size={100} />
          </div>
        ) : (
          <>
            <button className="back" onClick={() => handleClick()}>
              Back
            </button>
            {!location ? (
              <Error404 />
            ) : (
              <Card>
                <Grid container>
                  <Grid xs={12} md={6}>
                    <Grid style={{ margin: 20 }}>
                      <h2>{location.name}</h2>
                      <div>
                        <p>
                          <strong>Type: </strong>
                          {location.type ? location.type : 'Not specified'}
                        </p>
                        <p>
                          <strong>Dimension: </strong>
                          {location.dimension ? location.dimension : 'Not specified'}
                        </p>
                      </div>
                    </Grid>
                  </Grid>
                  <Grid xs={12} md={6}>
                    <div className="limit">
                      {location.residents.length > 0 ? (
                        <p>
                          <div className="title">
                            <strong>Residents: </strong>
                          </div>
                          <List className="characters">
                            {location.residents.map((element, index) => (
                              <Link to={`/character/${element.match(/[0-9]+/)}`}>
                                <ListItem className={index % 2 ? 'second' : 'first'} button>{`Character ${element.match(
                                  /[0-9]+/,
                                )}`}</ListItem>
                              </Link>
                            ))}
                          </List>
                        </p>
                      ) : null}
                    </div>
                  </Grid>
                </Grid>
              </Card>
            )}
          </>
        )}
      </Wrapper>
    </LocationDetailStyle>
  )
}

export default LocationDetail
