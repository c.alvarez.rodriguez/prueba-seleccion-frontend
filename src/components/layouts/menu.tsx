import { IconButton, MenuItem } from '@material-ui/core'
import React, { useState } from 'react'
import styled from 'styled-components'
import MenuIcon from '@material-ui/icons/Menu'
import Menu from '@material-ui/core/Menu'
import { Redirect } from 'react-router-dom'

const CustomMenuStyle = styled.div``

const CustomMenu = () => {
  const [anchorEl, setAnchorEl] = useState(null)
  const [logStatus, setLogStatus] = useState(true)
  const open = Boolean(anchorEl)

  const handleMenu = (event: any) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const logout = () => {
    localStorage.removeItem('token')
    setLogStatus(false)
    handleClose()
  }

  return (
    <CustomMenuStyle>
      <IconButton
        aria-label="account of current user"
        aria-controls="menu-appbar"
        aria-haspopup="true"
        onClick={handleMenu}
        color="inherit"
      >
        <MenuIcon />
      </IconButton>
      <Menu
        id="menu-appbar"
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={open}
        onClose={handleClose}
      >
        <MenuItem onClick={() => logout()}>Logout</MenuItem>
        {logStatus ? null : <Redirect to="/login" />}
      </Menu>
    </CustomMenuStyle>
  )
}

export default CustomMenu
