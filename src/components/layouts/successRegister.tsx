import { Button } from '@material-ui/core'
import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const SuccessRegisterStyle = styled.div`
  max-width: 700px;
  padding: 50px;
  display: flex;
  flex-direction: column;
  margin-left: auto;
  margin-right: auto;
  h1 {
    font-size: 60px;
    text-align: center;
    margin: 20px 0;
  }
  h2 {
    font-size: 50px;
    text-align: center;
  }
  p {
    text-align: center;
    font-size: 20px;
  }
  .link {
    text-decoration: none;
  }
  button {
    background-color: #30b0c2;
    margin: 20px 0;
    font-weight: bold;
    color: white;
    transition: 0.3s;
    :hover {
      color: black;
      background-color: #c4ec57;
    }
  }
`

const SuccessRegister = () => {
  return (
    <SuccessRegisterStyle>
      <h1>¡Successful Registration!</h1>
      <p>The user has successfully registered.</p>
      <p>Enter to Login to sign in.</p>
      <Link className="link" to="/login">
        <Button type="submit" fullWidth variant="contained">
          Return to Login
        </Button>
      </Link>
    </SuccessRegisterStyle>
  )
}

export default SuccessRegister
