import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import Wrapper from '../wrappers/wrapper'
import HeaderLogo from '../../assets/Rick_and_Morty_logo.png'
import CustomMenu from './menu'

const HeaderStyled = styled.div`
  box-shadow: 0 2px 4px 0 rgb(0, 0, 0, 0.2);
  overflow: hidden;
  h1 {
    font-size: 14px;
  }
  .menu {
    cursor: pointer;
    display: inline-flex;
    margin-right: 10px;
  }
  .content {
    align-items: center;
    display: flex;
    height: 80px;
    justify-content: space-between;
    .logo {
      max-width: 200px;
    }
  }
  @media screen and (min-width: 768px) {
    h1 {
      font-size: 24px;
    }
    p {
      font-size: 1rem;
    }
  }
`

const Header = () => {
  return (
    <HeaderStyled>
      <Wrapper>
        <div className="content">
          <Link to="/home">
            <img className="logo" alt="R&M Logo" src={HeaderLogo} />
          </Link>
          <CustomMenu />
        </div>
      </Wrapper>
    </HeaderStyled>
  )
}

export default Header
