import React from 'react'
import styled from 'styled-components'
import PaginationMaterial from '@material-ui/lab/Pagination'

const CustomPaginationStyled = styled.div``

interface IProps {
  pages: number
  handlePagination: Function
}

const CustomPagination = (props: IProps) => {
  const { pages, handlePagination } = props

  return (
    <CustomPaginationStyled>
      <PaginationMaterial onChange={(event, val) => handlePagination(val)} count={pages} shape="rounded" variant="outlined" />
    </CustomPaginationStyled>
  )
}

export default CustomPagination
