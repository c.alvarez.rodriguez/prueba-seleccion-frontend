import * as Yup from 'yup'

export const LoginSchema = Yup.object().shape({
  email: Yup.string().email('This email is invalid').required('This field is required'),
  password: Yup.string().min(3, 'Password must have more than 3 letters').required('Campo requerido'),
})

export const RegisterSchema = Yup.object().shape({
  email: Yup.string().email('This email is invalid').required('This field is required'),
  password: Yup.string().min(3, 'Password must have more than 3 letters').required('Campo requerido'),
})
