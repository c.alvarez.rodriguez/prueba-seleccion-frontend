import React from 'react'
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom'
import './App.css'
import CharacterDetail from './components/details/characterDetail'
import EpisodeDetail from './components/details/episodeDetail'
import LocationDetail from './components/details/locationDetail'
import Error404 from './components/errors/error404'
import Header from './components/layouts/header'
import { PrivateRoute, PublicRoute } from './components/routes/CustomRoutes'
import Home from './views/home'
import Login from './views/login'
import Register from './views/register'

const App = () => {
  return (
    <div className="App">
      <Router>
        <Header />
        <Switch>
          <Route exact path="/">
            <Redirect to="/home" />
          </Route>
          <PublicRoute exact path="/login" component={Login} />
          <PublicRoute exact path="/register" component={Register} />
          <PrivateRoute exact path="/home" component={Home} />
          <PrivateRoute exact path="/character/:id" component={CharacterDetail} />
          <PrivateRoute exact path="/episode/:id" component={EpisodeDetail} />
          <PrivateRoute exact path="/location/:id" component={LocationDetail} />
          <Route component={Error404} />
        </Switch>
      </Router>
    </div>
  )
}

export default App
