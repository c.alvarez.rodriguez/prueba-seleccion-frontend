import axios from 'axios'

export const getCharacters = async (id: number) => {
  const options = {
    url: `${process.env.REACT_APP_API}${process.env.REACT_APP_CHARACTERS}?page=${id}`,
    headers: {
      'Content-Type': 'application/json',
    },
  }
  try {
    const { data } = await axios(options)
    return data
  } catch (error) {
    console.log(error)
  }
}

export const getCharacterById = async (id: number) => {
  const options = {
    url: `${process.env.REACT_APP_API}${process.env.REACT_APP_CHARACTERS}/${id}`,
    headers: {
      'Content-Type': 'application/json',
    },
  }
  try {
    const { data } = await axios(options)
    return data
  } catch (error) {
    console.log(error)
  }
}

export const getLocations = async (id: number) => {
  const options = {
    url: `${process.env.REACT_APP_API}${process.env.REACT_APP_LOCATION}?page=${id}`,
    headers: {
      'Content-Type': 'application/json',
    },
  }
  try {
    const { data } = await axios(options)
    return data
  } catch (error) {
    console.log(error)
  }
}

export const getLocationById = async (id: number) => {
  const options = {
    url: `${process.env.REACT_APP_API}${process.env.REACT_APP_LOCATION}/${id}`,
    headers: {
      'Content-Type': 'application/json',
    },
  }
  try {
    const { data } = await axios(options)
    return data
  } catch (error) {
    console.log(error)
  }
}

export const getEpisodes = async (id: number) => {
  const options = {
    url: `${process.env.REACT_APP_API}${process.env.REACT_APP_EPISODES}?page=${id}`,
    headers: {
      'Content-Type': 'application/json',
    },
  }
  try {
    const { data } = await axios(options)
    return data
  } catch (error) {
    console.log(error)
  }
}

export const getEpisodeById = async (id: number) => {
  const options = {
    url: `${process.env.REACT_APP_API}${process.env.REACT_APP_EPISODES}/${id}`,
    headers: {
      'Content-Type': 'application/json',
    },
  }
  try {
    const { data } = await axios(options)
    return data
  } catch (error) {
    console.log(error)
  }
}
