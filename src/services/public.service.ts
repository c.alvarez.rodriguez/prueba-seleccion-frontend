import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'
import { ILoginResponse, IUser } from 'src/models/users.mode'

export const postLogin = async (data: IUser) => {
  const options: AxiosRequestConfig = {
    url: `${process.env.REACT_APP_REQRES}/login`,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data,
  }
  return new Promise<ILoginResponse['token']>(async (resolve, reject) => {
    try {
      const response: AxiosResponse<ILoginResponse> = await axios(options)
      response.status !== 200 ? reject() : resolve(response.data.token)
    } catch (error) {
      reject()
    }
  })
}

export const postRegister = async (data: IUser) => {
  const options: AxiosRequestConfig = {
    url: `${process.env.REACT_APP_REQRES}/register`,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data,
  }
  return new Promise<ILoginResponse['token']>(async (resolve, reject) => {
    try {
      const response: AxiosResponse<ILoginResponse> = await axios(options)
      response.status !== 200 ? reject() : resolve(response.data.token)
    } catch (error) {
      reject()
    }
  })
}
